import React from 'react';

/* Cuando solo necesitamos un componente HTML que no necesita la lógica de React
no es necesario crear un componente de React. Podemos usar una función
"stateless" que solo tendrá un método render y ya está.
El <h3 className="tagline"><span>{ this.props.tagline }</span></h3> "this"
que referenciaba a React ya no hace falta, se cogen los props directamente
de la función.
*/

// class Header extends React.Component {
const Header = (props) => {
    return (
        <header className="top">
            <h1>Consejo
            <span className="ofThe">
                <span className="of">de</span>
                <span className="the">este</span>
                </span>
                Día</h1>
                <h3 className="tagline"><span>{ props.tagline }</span></h3>
                </header>
    )
}

// Para temas de validación de datos
Header.propTypes = {
    tagline: React.PropTypes.string.isRequired
}

export default Header;
