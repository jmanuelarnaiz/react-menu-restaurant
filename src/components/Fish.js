import React from 'react';
import { formatPrice } from '../helpers';

class Fish extends React.Component {
    render () {
        const { details, index } = this.props;
        // const details = this.props.details;
        // const index = this.props.index;
        const isAvailable = details.status === 'available';
        const buttonText = isAvailable ? 'Añadir al pedido' : '¡No disponible!';
        return (
            <li className="menu-fish">
                <img src={ details.image } alt={ details.desc } />
                <h3 className="fish-name">
                    { details.name }
                    <span className="price">{ formatPrice( details.price ) }</span>
                </h3>
                <p>{ details.desc }</p>
                <button disabled={ !isAvailable } onClick={ () => this.props.addToOrder(index) } >
                    { buttonText }
                </button>
            </li>
        );
    }
}

Fish.propTypes = {
    details:React.PropTypes.object.isRequired,
    index:React.PropTypes.string.isRequired,
    addToOrder:React.PropTypes.func.isRequired
}

export default Fish;

/*
Si lo dejamos así: onClick={ this.props.addToOrder(key) } , no se
"bindea", es decir, si hay algún cambio en la lista, no se refresca.
Hay dos formas: La primera es esta:
    this.props.addToOrder.bind(null, key);
    Y la segunda es la que dejamos:
*/
