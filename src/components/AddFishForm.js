import React from 'react';

class AddFishForm extends React.Component {

    createFish(event) {
        event.preventDefault();
        console.log('Pescaito frito');
        const fishDTO = {
            name: this.name.value,
            price: this.price.value,
            status: this.status.value,
            desc: this.desc.value,
            image: this.image.value
        }
        console.log("EL pescadito es: ", fishDTO);

        this.props.addFish(fishDTO);
        this.fishForm.reset();
    }

    render() {
        return (
            <form ref={ (input) => this.fishForm = input } className="fish-edit" onSubmit={(e) => this.createFish(e)}>
                <input ref={ (input) => this.name = input } type="text" placeholder="Nombre del pescado" />
                <input ref={ (input) => this.price = input } type="text" placeholder="Precio del pescado" />
                <select ref={ (input) => this.status = input }>
                    <option value="available">¡Fresco!</option>
                    <option value="unavailable">Sin existencias</option>
                </select>
                <textarea ref={ (input) => this.desc = input } placeholder="Descripción del pescado" ></textarea>
                <input ref={ (input) => this.image = input } type="text" placeholder="Imagen del pescado" />
                <button type="submit">+ Añadir producto</button>
            </form>
        );
    }
}

AddFishForm.propTypes = {
    addFish: React.PropTypes.func.isRequired // Le pedimos que exista la función addFish
}

export default AddFishForm;
