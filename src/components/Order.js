import React from 'react';
import { formatPrice } from '../helpers';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'; // ES6

class Order extends React.Component {

    constructor() {
        super();
        this.renderOrder = this.renderOrder.bind(this);
    }

    renderOrder(key) {
        const fish = this.props.fishes[key];
        const count = this.props.order[key];
        const removeButton = <button onClick={ () => { this.props.removeFromOrder(key) } }>&times;</button>

        if (!fish || fish.status === 'unavailable') {
            return(
                <li key={ key }>Lo sentimos, {fish ? fish.name : 'ese producto' } ya no está disponible.
                    { removeButton }
                </li>
            );
        }

        return (
            <li key={ key }>
                <span>
                    <ReactCSSTransitionGroup
                        component="span"
                        className="count"
                        transitionName="count"
                        transitionEnterTimeout={250}
                        transitionLeaveTimeout={250}
                    >
                    { /* Cada vez que haya dos elementos uno pegado a otro, necesitan un "key". Para saber
                        a que span debo aplicarle una clase y a cual debo aplicarle otra */
                    }
                        <span key={ count }>{ count }</span>
                    </ReactCSSTransitionGroup>
                    x { fish.name } </span>
                <span className="price">{ formatPrice(count * fish.price) }</span>
                { removeButton }
            </li>
        );
    }

    render() {
        const orderIds = Object.keys(this.props.order);
        // Itera por key y se va sumando en prevTotal
        const total = orderIds.reduce( (prevTotal, key) => {
            const fish = this.props.fishes[key];
            const count = this.props.order[key];
            const isAvailable = fish && fish.status === 'available';
            if (isAvailable) {
                //order[key] = order[key] + 1 || 1; // Si ya existe le sumo 1, y en caso contrario, le asigno un 1.
                return prevTotal + (count * fish.price || 0);
                /* Puede que estando en el pedido pero esté borrado.
                Es decir, puede que exista 'fish' pero count no exista y
                daría error al intentar multiplicarlo. Por eso el || 0. */
            }
            return prevTotal;
        }, 0); // EL 0 es el valor inicial
        // Esto devolverá el array de precios actualizados
        return (
            <div className="order-wrap">
                <h2>Tu pedido</h2>
                <ReactCSSTransitionGroup
                    className="order"
                    component="ul"
                    transitionName="order"
                    transitionEnterTimeout={1000}
                    transitionLeaveTimeout={1000}>
                    { orderIds.map(this.renderOrder) }
                    <li className="total">
                        <strong>Total:</strong>
                        { formatPrice(total) }
                    </li>
                </ReactCSSTransitionGroup>
            </div>
        )
    }
}

Order.propTypes = {
    fishes:React.PropTypes.object.isRequired,
    order: React.PropTypes.object.isRequired,
    removeFromOrder:React.PropTypes.func.isRequired
}

export default Order;

/*
reduce -> Es como iterar sobre un array al que se le puede ir aplicando una función
por cada elemento.
https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/reduce
https://medium.freecodecamp.org/reduce-f47a7da511a9

*/
