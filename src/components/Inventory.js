import React from 'react';
import AddFishForm from './AddFishForm';
import base from '../base';

class Inventory extends React.Component {

    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
        this.renderInventory = this.renderInventory.bind(this);
        this.renderLogin = this.renderLogin.bind(this);
        this.authenticate = this.authenticate.bind(this);
        this.logout = this.logout.bind(this);
        this.authHandler = this.authHandler.bind(this);
        this.state = {
            uid: null,
            owner: null
        }
    }

    componentDidMount() {
        // TODO: Mostrar un mensaje rollo: "Comprobando credenciales..."
        base.onAuth( (user) => {
            if(user) {
                this.authHandler(null, { user });
            }
        });
    }

    handleChange(e, key) {
        const fish = this.props.fishes[key];
        // Aquí debemos obtener una copia del producto y actualizarlo con los nuevos datos
        const updatedFish = {
             ...fish,
             [e.target.name]: e.target.value
         } // Tomamos el fish y le cambiamos únicamente la proiedad que ha cambiado

         // Pero al escribir, no se modifica el input. Hay que pasarle el valor al componente App.

         this.props.updateFish(updatedFish, key);
        // console.log(e.target.name, e.target.value);
    }

    authenticate() {
        console.log('Intentando hacer Login en Facebook');
        base.authWithOAuthPopup('facebook', this.authHandler);
    }

    logout() {
        base.unauth();
        this.setState({ uid: null });
    }

    authHandler(error, authData) {
        console.log(authData);
        if (error) {
            console.error(error);
            return;
        }

        // Recuperar la información
        const storeRef = base.database().ref(this.props.storeId);

        // Hacer una consulta a Firebase para los datos de la tienda
        storeRef.once('value', (snapshot) => {
            const data = snapshot.val() || {};
            console.log(data);

            // Si la tienda no tiene dueño, hacer dueño al usuario actual
            const uid = authData.user.uid;
            if (!data.owner) {
                storeRef.set({
                    owner: uid
                });
            }

            this.setState({
                uid: uid,
                owner: data.owner || uid
            });
        });

    }

    renderLogin() {
        return (
            <nav className="login">
                <h2>Inventario</h2>
                <p>Loguéate con Facebook para gestionar tu almacén</p>
                <button className="facebook" onClick={ () => this.authenticate() }>
                    Log In con Facebook
                </button>
            </nav>
        );
    }

    renderInventory(key) {
        const fish = this.props.fishes[key];
        return (
            <div className="fish-edit" key={ key }>
                <input type="text" name="name" value={ fish.name }
                    placeholder="Nombre del producto"
                    onChange={ (e) => this.handleChange(e, key) } />
                <input type="text" name="price" value={ fish.price }
                    placeholder="Precio del producto"
                    onChange={ (e) => this.handleChange(e, key) } />
                <select type="text" name="status" value={ fish.status }
                    placeholder="Estado del producto"
                    onChange={ (e) => this.handleChange(e, key) }>
                    <option value="available">¡Fresco!</option>
                    <option value="unavailable">Sin existencias</option>
                </select>
                <textarea type="text" name="desc" value={ fish.desc }
                    placeholder="Descripción del producto"
                    onChange={ (e) => this.handleChange(e, key) } ></textarea>
                <input type="text" name="image" value={ fish.image }
                    placeholder="Fish Image"
                    onChange={ (e) => this.handleChange(e, key) }/>
                <button onClick={ () => this.props.removeFish(key) }>Eliminar producto</button>
            </div>
        );
    }

    render() {
        // Como no estamos pasando parámetros, el evento onClick pued reescribirse
        // de una manera más simple
        // const logout = <button onClick={ () => this.logout() }>Salir</button>;
        const logout = <button onClick={ this.logout }>Salir</button>;
        // Check if user is logged in
        if (!this.state.uid) {
            return <div>{ this.renderLogin() }</div>
        }

        // Check if user is the owner of the current store
        if (this.state.uid !== this.state.owner) {
            return (
                <div>
                    <p>Lo sentimos, no tienes acceso a este almacén</p>
                    { logout }
                </div>
            )
        }
        return (
            <div>
                <h2>Inventario</h2>
                { logout }
                {
                    Object
                    .keys(this.props.fishes)
                    .map(this.renderInventory)
                }
                <AddFishForm addFish={ this.props.addFish } />
                { /* Esto son los props que se le pasan desde App */ }
                <button onClick={ this.props.loadSamples }>Cargar productos de prueba</button>
            </div>
        );
    }

    static propTypes = {
        fishes:React.PropTypes.object.isRequired,
        storeId: React.PropTypes.string.isRequired,
        loadSamples:React.PropTypes.func.isRequired,
        addFish:React.PropTypes.func.isRequired,
        updateFish:React.PropTypes.func.isRequired,
        removeFish:React.PropTypes.func.isRequired
    }
}

export default Inventory;
