import React from 'react';
import { getFunName } from '../helpers';

class StorePicker extends React.Component {

    /*
    constructor() {
        super();
        this.goToStore = this.goToStore.bind(this);
    }
    */
    goToStore(event) {
        event.preventDefault();
        const storeId = this.storeInput.value;
        // Esto es para que no se envíe si no hay nada.
        this.context.router.transitionTo(`/store/${ storeId }`);
        //TODO: Grab the text from the box
        //TODO: Transition from / to /store/:storeId
    }

    render() {
        // Otro comentario
        // onSubmit={ this.goToStore.bind(this) }
        return (
            <form className="store-selector" onSubmit={ (e) => this.goToStore(e) }>
                { /* Aquí los comentarios se hacen así */ }
                <h2>Please enter a Store</h2>
                <input type="text" required placeholder="Store name"
                    defaultValue={ getFunName() }
                    ref={ (input) => this.storeInput = input }/>
                <button type="submit">Visit Store-></button>
            </form>
        )
    }
}


StorePicker.contextTypes = {
    router: React.PropTypes.object
}

export default StorePicker;
