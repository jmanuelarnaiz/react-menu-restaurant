import React from 'react';
import Header from './Header';
import Order from './Order';
import Inventory from './Inventory';
import Fish from './Fish';
import sampleFishes from '../sample-fishes';
import base from '../base';

class App extends React.Component {
    constructor() {
        super();

        this.addFish = this.addFish.bind(this);
        // Otra forma de "bindear" funciones, mirar la implementación
        // this.removeFish = this.removeFish.bind(this);
        this.loadSamples = this.loadSamples.bind(this);
        this.addToOrder = this.addToOrder.bind(this);
        this.removeFromOrder = this.removeFromOrder.bind(this);
        this.updateFish = this.updateFish.bind(this);

        /* En el constructor, no se puede usar this hasta que no llamemos a
        super(). State es para guardar el estado en el que se encuentran los
        datos de usuario */

        // Estado inicial
        this.state = {
            fishes: {},
            order: {}
        };
    }

    componentWillMount() {
        // Mirar en la pestaña 'React' de Chrome que App tiene un storeId en params, que viene de index.js
        this.ref = base.syncState(`${ this.props.params.storeId }/fishes`, {
            context: this,
            state: 'fishes' // Le puedo pasar lo que haya en el contenido de state, ya sea fishes u order
        });
        // Cuando cambiemos de 'store', habrá que dejar de sincronizar el actual

        // Comprobar si hay algún pedido ya existente en localStorage
        const localStorageRef = localStorage.getItem(`order-${ this.props.params.storeId }`);

        if (localStorageRef) {
            // Actualizar nuestro order state de App component
            this.setState( { order: JSON.parse(localStorageRef) } ); // Para pasar de String a Object
        }
    }

    componentWillUnmount() {
        base.removeBinding(this.ref);
    }

    componentWillUpdate(nextProps, nextState) {
        // Esto se ejecuta antes de que <App> sea renderizado.
        localStorage.setItem(`order-${ this.props.params.storeId }`, JSON.stringify(nextState.order));
    }

    addFish(fish) {
        // Update state. Primero guardar el estado en el que estaba y luego update
        const fishes = {...this.state.fishes}; // Esto es hacer una copia usando Spread operator. Ver más info debajo del todo
        // Luego, añadir nuestro nuevo pescado
        const timestamp = Date.now();
        fishes[`fish-${timestamp}`] = fish;
        /* TODO: Set state. No debemos pasarle todo el estado, solo aquello
        que se está modificando*/
        // this.setState({ fishes: fishes }); // <- Redundante, porque el nombre se llama igual
        this.setState( { fishes } );
        /* El setStates me imagino que es automático. Coge la propiedad fishes
        de state y le asigna el const fishes que hemos creado. */

    }

    updateFish(updatedFish, key) {
        const fishes = {...this.state.fishes };
        fishes[key] = updatedFish;
        this.setState( { fishes } );
    }

    removeFish = (key) => {
        const fishes = {...this.state.fishes };
        // Se podría hacer así delete thisIsObject['Cow'];, pero a Firebase no le gusta
        fishes[key] = null;
        this.setState( { fishes } );
    }

    loadSamples() {
        this.setState( { fishes: sampleFishes } );
    }

    addToOrder(key) {
        // Take a copy of our state
        const order = { ...this.state.order };
        // Update or add the new number of fish ordered
        order[key] = order[key] + 1 || 1; // Si ya existe le sumo 1, y en caso contrario, le asigno un 1.
        // Por último, update our State
        this.setState( { order } );
    }

    removeFromOrder(key) {
        const order = { ...this.state.order };
        // delete order[key];

        order[key]--;

        if (!order[key]) {
            delete order[key];
        }
        this.setState( { order } );
    }

    render() {
        return (
            <div className="catch-of-the-day">
                <div className="menu">
                    <Header tagline="Pescadería" openFor="432" mola={true}/>
                        <ul className="list-of-fishes">
                            {
                                Object
                                .keys(this.state.fishes)
                                .map( (key) => <Fish key={ key } index={ key } details={ this.state.fishes[key] } addToOrder={ this.addToOrder } /> )
                                // Le paso como props cada key de los peces y los detalles
                            }
                        </ul>
                </div>
                <Order
                    fishes={ this.state.fishes }
                    order={ this.state.order }
                    removeFromOrder={ this.removeFromOrder }
                    params={ this.props.params }
                />
            { /* Esto de pasar los params como prop lo hago para poder hacer en el componentWillUpdate this.props.params.storeId */ }
                <Inventory
                    addFish={ this.addFish }
                    updateFish={ this.updateFish }
                    removeFish={ this.removeFish }
                    loadSamples={ this.loadSamples }
                    fishes={ this.state.fishes }
                    storeId={ this.props.params.storeId }
                    />
            </div>
        )
    }
}

App.propTypes = {
    params: React.PropTypes.object.isRequired
}

export default App;

/*

    La lista de productos es mejor sincronizarla con Firebase. Pero con carácter
    didáctico, vamos a almacenar el pedido en el LocalStorage.

*/

/*
    Puede parecer estúpido tener dos atributos para pasar lo mismo, pero
    key es lo que usa React para las listas y no podemos pasarlo a otros componentes.
    Si queremos pasarlo, hemos de usar otro atributo, por eso hemos definido
    ese index.

    https://reactjs.org/docs/lists-and-keys.html

*/

/*

    SPREAD Operator: https://reactjs.org/docs/jsx-in-depth.html#spread-attributes

Digamos que es un operador de conversión que convierte un objeto de "props"
(pares clave-valor) en obeto JSX, para pasarlo por ejemplo en un "return".
Un prop son los datos de entrada a un componente: https://reactjs.org/docs/components-and-props.html
Estas dos funciones son equivalentes:

function App1() {
  return <Greeting firstName="Ben" lastName="Hector" />;
}

function App2() {
  const props = {firstName: 'Ben', lastName: 'Hector'};
  return <Greeting {...props} />;
}

Otro ejemplo más:

// Imaginemos que tenemos un objeto person de esta forma
var person= {
    name: 'Alex',
    age: 35
}
This:

<Modal {...person} title='Modal heading' animation={false} />
is equal to

<Modal name={person.name} age={person.age} title='Modal heading' animation={false} />

*/
