import Rebase from 're-base';

const base = Rebase.createClass({
    apiKey: "AIzaSyAblL_F7cV39_hGC_WOXT6GcLS0803ZOUk",
    authDomain: "ejemplo-react-pescaderia.firebaseapp.com",
    databaseURL: "https://ejemplo-react-pescaderia.firebaseio.com",
});

export default base;

// Como integrar Firebase en React: https://coderjourney.com/tutorials/how-to-integrate-react-with-firebase/
